#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/types.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include<numeric>
#include "visual_servoing.h"
#include "opencv2/imgcodecs.hpp"
#include <std_msgs/Float64MultiArray.h>
#include "longitudinal_control.h"
using namespace std;
class artery_detection
{
    ros::NodeHandle nh_;
    cv::Mat img;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_ ; 
    image_transport::Publisher image_pub_, image_pub_crop_;
    image_transport::Publisher conf_pub_;
    image_transport::Publisher cropped_pub_;
    ros::Publisher position_pub_, rot_pub_ ;
    ros::Publisher error_pub_;
    cv_bridge::CvImagePtr cv_ptr;
    float alpha = 0.121;
    float beta = 0.117;
    //float lambda = 0.08;
    float lambda = 0.05;//0.05;
    float lambda_o = 0.08;// 0.1;
    float dt = 0.1;
    float thresh = 4;
    int dir = 1;
    int contour_pre = 0;
    bool stop = false;
    Eigen::VectorXf position;
    std_msgs::Float64MultiArray pose_msg,rot_msg;
    public: 
    artery_detection (): it_(nh_)
    {
        
        image_sub_ = it_.subscribe("/frames", 1, &artery_detection::imgCallback, this);
        image_pub_= it_.advertise("/longitudinal_detection",1);
        image_pub_crop_= it_.advertise("/cropped",1);
        position_pub_ = nh_.advertise<std_msgs::Float64MultiArray>("/Position", 1);
        rot_pub_ = nh_.advertise<std_msgs::Float64MultiArray>("/Orientation", 1);
    }
    void imgCallback(const sensor_msgs::ImageConstPtr& msg)
    {
        try 
        {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch(cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception %s", e.what());
        } 

        longitudinal_control lc(alpha, beta, lambda_o);
        visual_servoing vs(alpha, lambda, dt);
        vs.crop(cv_ptr->image);
        sensor_msgs::ImagePtr img_out = cv_bridge::CvImage(std_msgs::Header(), "bgr8", vs.img).toImageMsg();
        image_pub_crop_.publish(img_out);
        vs.longitudinal_feature_extraction();
        sensor_msgs::ImagePtr msg_out = cv_bridge::CvImage(std_msgs::Header(), "bgr8", vs.img).toImageMsg();
        image_pub_.publish(msg_out);
        rot_msg.data.resize(2);
        position.resize(6,1);
        vs.control(position);
        pose_msg.data.resize(7);
        pose_msg.data[0] = position(0);
        pose_msg.data[1] = position(1);
        pose_msg.data[2] = position(2);
        pose_msg.data[3] = 0;
        pose_msg.data[4] = 0;
        pose_msg.data[5] = 0;
        pose_msg.data[6] = 0;
        cv::Point target_or;
        target_or.x = vs.img.size[1];
        target_or.y = vs.rotation_input.y;
        float theta_z_t = lc.theta_compute(target_or);
        cout<<"[Info] theta_z_t : "<<theta_z_t<<endl;//targetted  theta_z
        float theta_z_c = lc.theta_compute(vs.rotation_input);//current thetaZ
        cout<<"[Info] theta_z_c : "<<theta_z_c<<endl;
        float theta_out;
        if (vs.detect_flag)
        {
            pose_msg.data[5] = -lc.control(theta_z_c, theta_z_t);
        }
    
        if (lc.reached)
        {
            cout<<"[REACHED]"<< endl;
            cout<< lc.stop<< endl;
            if (stop)
            {
                cout<<"[STOP]"<< endl;
                pose_msg.data[0] = 0;
                pose_msg.data[1] = 0;
                pose_msg.data[2] = 0;
                pose_msg.data[3] = 0;
                pose_msg.data[4] = 0;
                pose_msg.data[5] = 0;
                pose_msg.data[6] = 2; 
            }
            if (!stop)
            {
                cout<<"[NO STOP]"<< endl;
                cout<<vs.area<< " , " << contour_pre<<endl;
                pose_msg.data[5] = -dir*lc.delta_theta;
                dir = lc.areaControl(contour_pre, vs.area);
                contour_pre = vs.area;
                stop = lc.stop;
                
            }
        }
        position_pub_.publish(pose_msg);
        
    }



};
int main(int argc, char** argv)
{
    ros::init(argc , argv, "artery_detection");
    cout<<"artery_tracking"<<endl;
    artery_detection aT;
    ros::spin();

    return 0;
}