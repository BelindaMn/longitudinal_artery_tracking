#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
#include <std_msgs/Float64MultiArray.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/mat.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include <string>
#include "visual_servoing.h"
#include "signalProcessing.h"
#include <fstream>



using namespace cv;
using namespace std;
/*start by testing
     -the average calculation (done and working)
     -plot the vertical signal
     -peaks detection 
     -best VIP id
on images (not rosbag))*/

int main(int argc, char** argv)
{  
    clock_t start, end;
    int N = 10;
    visual_servoing vs;
    Mat image = imread ("/home/beli/catkin_ws/src/longitudinal_artery_tracking/bagfile/frame0221.jpg");
    vs.crop(image);
    Mat img , var_signal ;
    vector <int> best_vip_lpeak, best_vip_rpeak, lumen_up, lumen_down, min_cf2;

    cv::GaussianBlur(vs.img, img, cv::Size(27,27), 0);
    Mat sharpening_kernel = (Mat_<double>(3,3)<< 0,-1, 0,
                                                -1, 7,-1,
                                                 0,-1, 0);
    filter2D(img, vs.img, -1, sharpening_kernel);
    vector<double> signal_v, vip_signal;
    vector<vector<double> > vip_samples;
    
    signalProcessing process;
    // We extract the VIPs
    for(int j = 0; j< img.size[1]; j+= 10)
    {
        //cout<< img.size[0]<< " " << img.size[1]<< endl;
        for (int i = 0; i< img.size[0]; i ++)
        {
            signal_v.push_back(double(img.at<uchar>(i,j)));
        }

        
        vip_samples.push_back(signal_v);
        signal_v.clear();
    }
    //double execution_time = double(end-start)/ double(CLOCKS_PER_SEC);
    //imshow("Window Name", vs.img);
    //waitKey(0);

    vector<double> mean_signal;
    for (int i = 0; i<vip_samples.size(); i++)
    {
        vector<double> mean_kernel (N, 1/double(N) );
        auto iter = max_element(vip_samples[i].begin(), vip_samples[i].end());
        auto max = *iter;
        iter = min_element(vip_samples[i].begin(), vip_samples[i].end());
        auto min = *iter;
        for (auto& element:vip_samples[i])
        {
            vip_signal.push_back((element-min)/(max - min));
        }
        
        process.convolve1D(vip_signal, mean_kernel, mean_signal);

        process.best_vip_id(mean_signal);

        best_vip_lpeak = process.best_vip_ids;
        process.peak_ids.clear();
        vector<double> inverse_mean_signal = mean_signal;
        sort(inverse_mean_signal.begin(), inverse_mean_signal.end(), greater<>() );
        process.best_vip_id(inverse_mean_signal);
        vector<int> inverse_peaks;
        inverse_peaks = process.peak_ids;
    
        for(auto& ip : inverse_peaks)
        {
            best_vip_rpeak.push_back(mean_signal.size()-ip);
        }
        sort (best_vip_rpeak.begin(), best_vip_rpeak.end(), greater<>());
        //cout<<best_vip_rpeak.size()<<endl;
        int mid_peak, start_peak, end_peak;
        process.min_derivatives(vip_signal, best_vip_lpeak, best_vip_rpeak, mid_peak, start_peak, end_peak);

        best_vip_lpeak.clear();
        best_vip_rpeak.clear();
        process.peak_ids.clear();
        mean_signal.clear();
        vip_signal.clear();
    }
    

    
}