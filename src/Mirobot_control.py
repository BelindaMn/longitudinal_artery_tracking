from cmath import cos
from turtle import position
from wlkata_mirobot import WlkataMirobot, WlkataMirobotTool
import time
import rospy 
from std_msgs.msg import Float64MultiArray
import numpy as np
from math import *

class karl_control ():
    def __init__(self) :
        self.p = np.zeros((7,1))
        self.theta_z  = np.array([0,0]) 
        self.flag = 0       
        self.i = 0
        self.rot = 0
ctrl = karl_control()

def start_node():
    
    rospy.init_node("Mirobot")
    pose_pub = Float64MultiArray()
    pub = rospy.Publisher("/current_position", Float64MultiArray, queue_size=1)
    rate = rospy.Rate(10) #10Hz : 0.1s
    while not rospy.is_shutdown():
        
        rospy.Subscriber("/Position", Float64MultiArray, execution, queue_size=1)
        pose_pub.data = [m.pose.x,m.pose.y,m.pose.z,m.pose.roll,m.pose.pitch,m.pose.yaw]
        pub.publish(pose_pub)
        rospy.Subscriber("/Orientation", Float64MultiArray, get_orientation, queue_size=1)
        if ctrl.p[6] == 0 :
            print(ctrl.flag)
            print(m.pose.yaw)
            pose_pre = [m.pose.x,m.pose.y,m.pose.z,m.pose.roll, m.pose.pitch, m.pose.yaw]

            m.set_tool_pose(pose_pre[0]+ctrl.p[0], roll = 0 , pitch = 0,yaw = pose_pre[5]-ctrl.p[5] , mode='p2p',speed = 1000 ,is_relative=False, wait_ok=False)
            
        if ctrl.p[6] == 1 :
            m.set_tool_pose(ctrl.p[0], ctrl.p[1], ctrl.p[2] ,roll=ctrl.p[3] , pitch =ctrl.p[4], yaw = ctrl.p[5], mode='p2p',speed = 1000 ,is_relative=False, wait_ok=False)
            position_centered = [m.pose.x,m.pose.y,m.pose.z]
            ctrl.flag = 0
        if ctrl.p[6] == 2 :
            m.set_tool_pose(ctrl.p[0], ctrl.p[1], ctrl.p[2] ,roll=ctrl.p[3] , pitch =ctrl.p[4], yaw = ctrl.p[5], mode='p2p',speed = 1000 ,is_relative=True, wait_ok=False)
            position_centered = [m.pose.x,m.pose.y,m.pose.z]
            ctrl.flag = 0
        #m.set_tool_pose(0, 0, 0 ,roll=0 , pitch =0,yaw = ctrl.theta_z, mode='p2p',speed = 2500 ,is_relative=True, wait_ok=False)
    #print(ctrl.p)
        rate.sleep()      
        print(ctrl.p)
        print(ctrl.theta_z)
        
def execution(pos):
    try :
        ctrl.p = pos.data
        ctrl.flag = 0
    except Exception as err :
        print(err)

def get_orientation(orientation):
    ctrl.theta_z[0]= orientation.data[0]
    ctrl.flag = 1

if __name__ == "__main__":
    try : 
        m = WlkataMirobot(wait_ok= True)
        m.home() 
        m.set_tool_offset(offset_x=0, offset_y=55, offset_z= 40, wait_ok=True)
        start_node()
    except Exception as err:
        print(err)