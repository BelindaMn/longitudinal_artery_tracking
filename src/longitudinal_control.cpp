#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/types.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include<numeric>
#include "opencv2/imgcodecs.hpp"
#include <std_msgs/Float64MultiArray.h>
#include "longitudinal_control.h"

longitudinal_control::longitudinal_control(float Alpha, float Beta, float Lambda)
{
    alpha = Alpha;
    beta = Beta;
    lambda = Lambda;
}
float longitudinal_control::theta_compute(cv::Point rot_input)
{
    float theta = 0;
    cv::Point left, right;
    float r = sqrt(pow(alpha*(rot_input.x - 162), 2) + (pow(beta*rot_input.y, 2)));
    theta   = atan2(alpha*(rot_input.x-162)/r , beta*rot_input.y/r);
    return theta *180 /M_PI;
}
float longitudinal_control::control(float theta_current, float theta_target )
{
    
    std::cout<<theta_current<<std::endl;
    int thresh = 0.05;
    std::cout<<reached<<std::endl;
    if(abs(theta_current - theta_target)> thresh)
    {
        
        theta_pre = theta_current;
        reached = 0;
        return -lambda*(theta_target-theta_current);
    }    
    else
    {
        std::cout<<"reached" << std::endl;
        reached = 1;
        return 0;
    }
    
}
float longitudinal_control::areaControl(int contour_pre, int contour_current)
{
    if (contour_pre - contour_current< 0)
    {   
        stop = 0;
        std::cout<<"keep turning"<< std::endl;
        return 1;
    }
    if (contour_pre - contour_current> 0) 
    {   std::cout<<"We can stop"<< std::endl;
        stop = 1;
        return -1;
    }
    if (contour_pre - contour_current == 0)
    {
        std::cout<<"same area"<<std::endl;
        return 0;
    }

}