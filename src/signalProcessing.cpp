#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/mat.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include "signalProcessing.h"
#include<numeric>
#include<algorithm>
#define none 20000

using namespace std;
signalProcessing::signalProcessing()
{
    nb = 4;
}

double signalProcessing::signal_average(vector<double>& signal_in)
{
    nb_of_elements = signal_in.size();
    double sum = 0;
    //cout<< "[INFO] Number of elements in signal = " << nb_of_elements<<endl;
    vector<double>::iterator it ;
    for (it = signal_in.begin(); it < signal_in.end(); it++)
    {
        sum += *it;
    }
    return (sum/nb_of_elements);
}
double signalProcessing::computeQuantile(vector<double>& signal_in , double order)
{
    const auto n = signal_in.size();
    const auto id = (n-1) * order;
    const auto lo = floor (id);
    const auto hi = ceil(id);
    const auto qs = signal_in[lo];
    const auto h = (id - lo);
    
    return (1 -h) * qs + h * signal_in[hi];
}

void signalProcessing::peaks_detection(vector<double>& signal_in)
{
    int peakIndex = none;
    double peakValue = none;
    double baseline = computeQuantile(signal_in, 0.1);
    for (int i = 0; i < signal_in.size() ; i++)
    {
        if(signal_in[i] > baseline)
        {
            if (peakValue == none || signal_in[i]> peakValue)
            {
                
                peakIndex = i;
                peakValue = signal_in[i];
                peak_ids.push_back(peakIndex);
                peak_values.push_back(peakValue);
            }
        }
        else if (signal_in[i] < baseline)
        {
            peakIndex = none ; 
            peakValue = none;
        }
        
    }
}

void signalProcessing::argsort(vector<double>& vect_in, int n, vector<int>& index  )
{   
    vector<pair<int, int>> vp;
    for (int i = 0; i < n ; i++)
    {
        vp.push_back(make_pair(vect_in[i], i));
        sort(vp.begin(), vp.end());

    }
    for(int i = 0 ; i < vp.size(); i++)
    {
        index.push_back(vp[i].second);
    }
}

void signalProcessing::best_vip_id(vector<double>& signal_in)
{
    
    vector<int> cf1, peaks_id, valid_peaks,temp_int;
    vector<int>::iterator p, second_p, it;
    vector<double> signal_seg, temp, scaled_seg, cf1_peak, cf1_seg;
    vector<double>::iterator iter;
    int peaks_distance;
    double q3,q1 ;
    double epsilone = 3;
    peaks_detection(signal_in);
    //cout<<peak_ids.size()<<endl;
    for (p = peak_ids.begin(); p < peak_ids.end(); p ++)
    {
        for(second_p = peak_ids.begin(); second_p < peak_ids.end(); second_p ++)
        {
            peaks_distance = abs(*second_p - *p);
            if (peaks_distance < 200 && peaks_distance > 50 )
            {
                valid_peaks.push_back(*second_p);
            } 
        }
        for (second_p = valid_peaks.begin(); second_p < valid_peaks.end(); second_p ++ )
        {
            int len_template = int( 3 * abs(*second_p - *p) /4);
            int depth_template = int(abs(*second_p - *p)/4);
            signal_seg = {signal_in.begin()+*p , signal_in.begin()+*p + len_template};
            //cout<<"{}"<<signal_seg.size()<<endl;
            q3 = computeQuantile(signal_in, 0.75);
            
            //if the average is > than the VIP 75 th quantile 
            if (signal_average(signal_seg) > q3) continue;
            
            for (int i = 0 ; i < signal_seg.size(); i++)
            {
                if (signal_seg[i] > 0) temp.push_back(signal_seg[i]);
                else continue;
            }
            signal_seg.clear();
            signal_seg = temp;
            temp.clear();
            
            q1 = computeQuantile(signal_seg, 0.1);
            //cout<<"{}"<<signal_seg.size()<<endl;
            
            for (int i = 0; i < signal_seg.size(); i++)
            {
                if (abs(double(signal_seg[i] - q1)) <= epsilone)
                {   
                    //cout<<signal_seg[i]-q1<<endl;
                    temp_int.push_back(i);
                } 
                else
                {
                    
                    continue;
                } 
            }
            

            try{
                it = max_element(temp_int.begin(), temp_int.end());
                len_template = *it;
                
            }
            catch (int e)
            {
                continue;
            }
            if ((len_template < (depth_template+5)) || depth_template < 10 ) continue;

            temp = {signal_seg.begin(), signal_seg.begin() + len_template+1};
            signal_seg.clear();
            signal_seg = temp;
            temp.clear();
            iter = max_element(signal_seg.begin(), signal_seg.end());
            auto max = *iter;
            iter = min_element(signal_seg.begin(), signal_seg.end());
            auto min = *iter;
            for (auto& element : signal_seg )
            {
                scaled_seg.push_back((element - min) / (max-min));
            }
            for(auto& element : scaled_seg)
            {
                if (element > 0) temp.push_back(element);
            }
            scaled_seg.clear();
            scaled_seg = temp;
            temp.clear();
            q1 = computeQuantile(scaled_seg, 0.05);
            q3 = scaled_seg[0];

            //objective function 1 estimation
            for (int d = -10 ; d < 0 ; d ++)
            {
                vector<double> left_template(len_template, q1);
                vector<double> diff;
                temp.resize(depth_template+d);
                iota(begin(temp), end(temp), 0);
                sort(temp.begin(), temp.end(), greater<>());
                for (int i = 0; i < (depth_template + d) ; i++)
                {
                    left_template[i] = q1 + temp[1] * ((q3-q1) / (depth_template + d -1));
                    diff.push_back(signal_seg[i]-left_template[i]);
                }
                cf1_peak.push_back(accumulate(diff.begin(), diff.end(),0)/len_template);
                diff.clear();
                temp.clear();
            }
            if(cf1_peak.size()>0)
            {
                iter = min_element(cf1_peak.begin(), cf1_peak.end());
                cf1_seg.push_back(*iter);
                peaks_id.push_back(*p);
            }
            temp_int.clear();

            signal_seg.clear(); 
            cf1_peak.clear();
        }
        valid_peaks.clear();
    }
    vector<int> ordered_cf1;
    argsort(cf1_seg, cf1_seg.size() , ordered_cf1);
    if (ordered_cf1.size()> nb)
    { 
        temp_int ={ ordered_cf1.begin(), ordered_cf1.begin()+nb};
        vector <int> temp_2;
        for(auto& element : temp_int)
        {
            temp_2.push_back(element);
        }

        sort(temp_2.begin(), temp_2.end());
        best_vip_ids = temp_2;
    }
    else
    {
        best_vip_ids = peaks_id;
    }
    
}
void signalProcessing::convolve1D(vector<double>& signal, vector<double>& kernel, vector<double>& result)
{
    int const n_signal = signal.size();
    int const n_kernel = kernel.size();
    int const n = n_signal + n_kernel-1;
    for (auto i = 0; i<n; i++)
    {
        result.push_back(0);
    }
    for (auto i = 0; i<n; i++)
    {
        int const jmn = (i >= n_kernel - 1)? i - (n_kernel - 1) : 0;
        int const jmx = (i <  n_signal - 1)? i            : n_signal - 1;
        for(auto j(jmn); j <= jmx; ++j) 
        {
            result[i] += (signal[j] * kernel[i - j]);
        }
    }
}
void signalProcessing::min_derivatives(vector<double>& signal_in, vector<int>& start_peaks, vector<int>& end_peaks, int mid_peak, int new_start_peak, int new_end_peak)
{
    int N = 5;
    vector<double> mean_kernel (N, 1/(double(N)));
    vector<double> laplacian = {1,-2,1};
    vector<double> mean_signal;
    convolve1D(signal_in, mean_kernel, mean_signal);
    vector<double> deriv_signal, temp, temp_1;

    convolve1D(mean_signal, laplacian, deriv_signal);
    int min_cf2 = none;
    new_start_peak = none;
    mid_peak = none;
    new_end_peak = none;
    int cf2 = 1000;
    
    for (auto& lp:start_peaks)
    {
       for (auto& rp:end_peaks)
       {
        cout<< rp - lp<< endl;
        if (((rp-lp) >50) && ((rp-lp)< 200))
        {
            
            for (int i = lp; i< rp; i ++)
            {
                temp.push_back(signal_in[i]);
            }
            auto q2=computeQuantile(temp, 0.05);
            for (auto& e : temp)
            {
                if ((e-q2)< 0.1)temp_1.push_back(e);
            }
            auto it = max_element(temp_1.begin(), temp_1.end());
            auto max = *it;
            it = min_element(temp_1.begin(), temp_1.end());
            auto min = *it;
            auto start_index = min + lp;
            auto end_index = max +lp ; 
            double cf2_new = 0;
            for (int i = start_index; i < end_index; i++)
            {
                cf2_new += abs(deriv_signal[i]);
            }
            cf2_new = cf2_new/(end_index - start_index);
            cout<<cf2_new<<endl;
        }
       }
       temp.clear();
       temp_1.clear();
    }
}