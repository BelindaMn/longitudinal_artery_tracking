from mimetypes import init
import time
import rospy 
from std_msgs.msg import Float64MultiArray
import numpy as np
from math import *

class orientation():
    def __init__(self):
        self.theta_z = 0
        self.target = -90
        self.kp = 0.1
        self.error = 0

def orientation_node():
    pub = rospy.Publisher ( 'Orientation', Float64MultiArray, queue_size = 1)
    rospy.init_node("Orientation_pub")
    rospy.Subscriber("/current_position", Float64MultiArray, get_current_position, queue_size=1)
    rospy.Subscriber("/Error", Float64MultiArray, get_error, queue_size=1)
    rate = rospy.Rate(5)
    data_out = Float64MultiArray()
    data_out.data = [-90]
    pub.publish(data_out)
    """while not rospy.is_shutdown():
        if abs(orient.error) <4  : 
            data_out.data = [-90]
            pub.publish(data_out)
        if abs(orient.error) > 4  : 
            data_out.data = [0]
            pub.publish(data_out)
        #print(orient.theta_z)

        rate.sleep()"""

def get_current_position (orientation_in):
    orient.theta_z = orientation_in.data[5]
    print (orientation_in.data)

def get_error (input):
    orient.error = input.data[1]

if __name__ == "__main__":
    try:
        orient = orientation()
        orientation_node()
    except Exception as err:
        print(err)