#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/mat.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include<numeric>
using namespace std;
class signalProcessing {
    public:
        int nb;
        int nb_of_elements;
        vector<int> peak_ids;
        vector<int> best_vip_ids;
        vector<double> peak_values;
        signalProcessing();
        void image_preprocessing(cv::Mat& img_in);
        double signal_average(vector<double>& signal_in);
        void argsort(vector<double>& vect_in, int n, vector<int>& index  );
        double computeQuantile(vector<double>& signal_in , double order);
        void peaks_detection(vector<double>& signal_in);
        void best_vip_id(vector<double>& signal_in);
        void convolve1D(vector<double>& signal, vector<double>& kernel, vector<double>& result);
        void min_derivatives(vector<double>& signal_in, vector<int>& start_peaks,  vector<int>& end_peaks, int mid_peak, int new_start_peak, int new_end_peak);

       


};