#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/types.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include <eigen3/Eigen/Dense>
#include<numeric>
#include "opencv2/imgcodecs.hpp"
#include <std_msgs/Float64MultiArray.h>

class longitudinal_control
{
    public :
        float alpha, beta, lambda; 
        cv::Point pt;
        bool reached = 0;// flag is raised if the rectangle border touches the image border, once it is raised, we switch to area control
        int pre_area = 0;
        bool stop;
        float delta_theta = 1;
        float current_theta_z, target_theta_z, theta_pre;
        longitudinal_control(float Alpha, float Beta, float Lambda);
        float theta_compute(cv::Point rot_input);
        float control(float theta_current, float theta_target);
        float areaControl(int contour_pre, int contour_current);

};